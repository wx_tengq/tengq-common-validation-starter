package cn.unboundary.common.validation.group;

import javax.validation.groups.Default;

/**
 * 校验分组
 */
public interface ValidationGroup {
    /**
     * 插入校验组
     */
    interface INSERT extends Default{}

    /**
     * 更新校验组
     */
    interface UPDATE extends Default{}
}
